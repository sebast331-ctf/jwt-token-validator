import base64
import json
import requests
from jwt import (
    JWT,
    jwk_from_dict
)

HOST = 'http://localhost:5000'


def get_signing_key():
    with open('./private.key') as f:
        key = json.load(f)
    return jwk_from_dict(key)


def get_public_key(host=HOST):
    r = requests.get(f'{host}/static/public.key')
    return jwk_from_dict(r.json())


def generate_token(name):
    instance = JWT()
    message = {
        'iss': f'{HOST}/',
        'data': {
            'username': name,
            'role': 'user'
        }
    }
    return instance.encode(message, get_signing_key(), alg='RS256')


def token_to_dicts(token):
    header, payload, signature = token.split('.')
    j_header = json.loads(base64.b64decode(header + '=' * (len(header) % 4)))
    j_payload = json.loads(base64.b64decode(payload + '=' * (len(payload) % 4)))
    return j_header, j_payload, signature


# Valider le token avec l'algo. Vulnérable pour algo=none.
def validate_token_2(token):
    instance = JWT()
    try:
        instance.decode(token, get_signing_key(), do_verify=True, algorithms=['RS256', 'none'])
        return True
    except Exception as err:
        return False


def validate_token_3(token):
    instance = JWT()
    header, payload, signature = token_to_dicts(token)
    try:
        instance.decode(token, get_public_key(payload['iss']), do_verify=True, algorithms=['RS256'])
        return True
    except Exception as err:
        return False

if __name__ == '__main__':

    instance = JWT()

    message = {
        'iss': 'http://localhost:8000',
        'sub': 'seb',
        'data': {
            'user': 'bob',
            'role': 'user'
        }
    }

    signing_key = jwk_from_dict(requests.get('http://localhost:8000/private.key').json())

    compact_jws = instance.encode(message, signing_key, alg='RS256')
    print(compact_jws)


    altered_message = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiAiaHR0cDovL2xvY2FsaG9zdDo4MDAwIiwgInN1YiI6ICJzZWIiLCAiZGF0YSI6IHsidXNlciI6ICJib2IiLCAicm9sZSI6ICJhZG1pbiJ9fQ.QUumq1Az9tP0barg6vQ8SFPnnSpMO_mXsFOGOKoqtxp4v_ggH7AQaI-cvg1eEFBl6A7zvpvWFIqZ57Vya3lqYillo-jmyd842AmpvSa5mxv3qAaPvDoPMrJBC2H25tP1vlHC02yTDvvDryAYjJd2rvZ7X6qaL71gUHHARPq_ue9K9CUFjbvFixDDIAkHoL-UMja1Qaxo4Ur88VlOXj7Zn6Jm2ltvbSUe12iLORQsvxmT5FsStPEFTK1U8UsnSnu_Noqa6DimqGYealSEW-8uGx9lslI5D1hDXadBJcGP7MVXgv99g6nO5APuYgzYaYsysNxsSnaLFNBRcOW0FGoXiw"
    message_received = instance.decode(altered_message, do_verify=False)
    # public_key = jwk_from_dict(requests.get('http://localhost:8000/public.key').json())
    # message_received = instance.decode(
    #     altered_message, public_key
    # )
    print(message_received)